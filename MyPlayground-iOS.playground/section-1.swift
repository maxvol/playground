// Playground - noun: a place where people can play

import XCPlayground
import UIKit
import SceneKit
import SpriteKit

var str = "Hello, playground"

var view = UITextView(frame:CGRect(x:0, y:0, width:240, height:120))
view.backgroundColor = UIColor.brownColor()
view.textColor = UIColor.yellowColor()
view.text = str
view

func myfunc() -> (code: Int, message:NSString) {
    return (1, "one")
}

var tuple = myfunc()
var msg = tuple.1
msg

var dinges = [1,2,3]
var resultaat = dinges.map {
    $0 * $0
}
resultaat

var f = dinges.filter {
    0 != $0 % 2
}
f

var r = dinges.reduce(0,+)
r

struct Transaction {
    let amount: Float
    let date: NSDate
    let name: NSString
}

let s = Transaction(amount: 37.8, date: NSDate(timeIntervalSinceNow: 1323523), name: "boo")

let transactions = [
    Transaction(amount: 37.8, date: NSDate(timeIntervalSinceNow: 13223), name: "Tax"),
    Transaction(amount: 33.8, date: NSDate(timeIntervalSinceNow: 13235), name: "Boe"),
    Transaction(amount: 27.8, date: NSDate(timeIntervalSinceNow: 13523), name: "Tax"),
    Transaction(amount: 367.8, date: NSDate(timeIntervalSinceNow: 1353), name: "Tax"),
    Transaction(amount: 1.8, date: NSDate(timeIntervalSinceNow: 33523), name: "Bah"),
    Transaction(amount: 667.8, date: NSDate(timeIntervalSinceNow: 3223), name: "Tax")
]
transactions

var t = transactions.filter {
    $0.name.isEqual("Tax")
}
t

var a = t.map {
    $0.amount
}

var sum = a.reduce(0, +)
sum

class MyScene : SKScene {
    init(size: CGSize) {
        super.init(size:size)

        self.backgroundColor = UIColor.brownColor()

        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        
        myLabel.color = UIColor.yellowColor()
        myLabel.text = "Hello, World!"
        myLabel.fontSize = 30
        myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
            CGRectGetMidY(self.frame))
        
        self.addChild(myLabel)
    }
    
}

// Configure the view.
let skView = SKView(frame: CGRect(x:0, y:0, width:240, height:120))
skView.showsFPS = true
skView.showsNodeCount = true

// Create and configure the scene.
let skScene = MyScene(size: skView.bounds.size)
skScene.scaleMode = .AspectFill

// Present the scene.
skView.presentScene(skScene);
skView





