// Playground - noun: a place where people can play

import XCPlayground
import Cocoa

var str = "Hello, playground"

for i in 1...10 {
    i*i
}

var image = NSImageNameAdvanced

var attrstr = NSAttributedString()
var view = NSTextView(frame:CGRect(x:0, y:0, width:240, height:120))
view.backgroundColor = NSColor.brownColor()
view.textColor = NSColor.yellowColor()

view

